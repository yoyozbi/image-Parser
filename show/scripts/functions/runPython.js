const {spawn} = require('child_process');
const {remote} = require('electron');
const path = require('path');
const fs = require('fs');
const pathToFile = remote.getGlobal("settings").pathToConvertImage;
const logOutput = (name) => (data) => console.log(`[${name}] ${data.toString()}`);
function run(file, param1,param2)
{
  var args = [param1];
  args.unshift(file);
  args.push(param2);
  const process = spawn('python' , args);
  process.stdout.on(
    'data',
    logOutput('stdout')
  );

  process.stderr.on(
    'data',
    logOutput('stderr')
  );
}

module.exports = {
  convertImage : function (path, to=pathToFile)
  {
    try
    {
      run('./scripts/convertMinImage.py',path, to)
    } catch (e)
    {
      console.error(e.stack);
      process.exit(1);
    }
  },

  convertBigImage : function(rankOfFiles,to=pathToFile)
  {
    var files = remote.getGlobal("sharedObj").files;
    try
    {
      run("./scripts/convertBigImage.py",files[parseInt(rankOfFiles)],to)
    } catch (e)
    {
      console.error(e.stack);
      process.exit(1);
    }
    var asFinish = false;
    while(asFinish === false)
    {
      if(fs.existsSync(to + path.basename(files[parseInt(rankOfFiles)],path.extname(files[parseInt(rankOfFiles)])) + '.jpg'))
      {
        asFinish = true;
      }
    }
  } 
}