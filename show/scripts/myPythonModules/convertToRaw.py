from PIL import Image
import rawpy
import imageio
from myPythonModules import pathProperty
from myPythonModules.other import alreadyExist

# -----------------------------------------------------------------------------
# -------------------convert raw image to a miniature of the image-------------
# ----------------------------------details--------------------------------------
# 1. if the destinationPath is not given the image will be save in the same folder
#    as the source image in jpg with the same name as source image but with a -min.
#
# 2. if the destination path is a folder the name of the image will be the same
#    as the source image
def minImage(sourcePath, destinationPath="."):
    finalName = findFinalName(sourcePath, destinationPath)
    img = Image.open(sourcePath)
    img.save(finalName)


def bigImage(sourcePath, destinationPath="."):
    finalName = findFinalName(sourcePath, destinationPath, False)
    with rawpy.imread(sourcePath) as raw:
        rgb = raw.postprocess()
    imageio.imsave(finalName, rgb)


def findFinalName(sourcePath, destinationPath=".", isMin=True):
    sourceFolder = pathProperty.folder(sourcePath)
    destinationFolder = pathProperty.folder(sourcePath)
    sourceFilename = pathProperty.filename(sourcePath)
    print("source folder : {}, source filename {}".format(sourceFolder, sourceFilename))
    finalName = ""
    if destinationPath == ".":
        finalName = sourceFolder + sourceFilename
        if isMin:
            finalName += "-min"
        finalName += ".jpg"
    elif destinationFolder or destinationFolder is False:
        asExtension = pathProperty.asExtension(destinationPath)
        if asExtension==False:
            if destinationFolder:
                finalName = destinationPath + sourceFilename
                if isMin:
                    finalName += "-min"
            finalName += ".jpg"
        else:
            finalName = destinationPath
    print(finalName)
    if alreadyExist(finalName) is False:
        return finalName
    else:
        return False
