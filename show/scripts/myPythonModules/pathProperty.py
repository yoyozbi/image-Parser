#             _                                     _   _
#            | |                                   | | (_)
#   __ _  ___| |_   _ __  _ __ ___  _ __   ___ _ __| |_ _  ___  ___
#  / _` |/ _ \ __| | '_ \| '__/ _ \| '_ \ / _ \ '__| __| |/ _ \/ __|
# | (_| |  __/ |_  | |_) | | | (_) | |_) |  __/ |  | |_| |  __/\__ \
#  \__, |\___|\__| | .__/|_|  \___/| .__/ \___|_|   \__|_|\___||___/
#   __/ |          | |             | |
#  |___/           |_|             |_|
#         __         _                               _   _
#        / _|       (_)                             | | | |
#   ___ | |_    __ _ ___   _____ _ __    _ __   __ _| |_| |__
#  / _ \|  _|  / _` | \ \ / / _ \ '_ \  | '_ \ / _` | __| '_ \
# | (_) | |   | (_| | |\ V /  __/ | | | | |_) | (_| | |_| | | |
#  \___/|_|    \__, |_| \_/ \___|_| |_| | .__/ \__,_|\__|_| |_|
#               __/ |                   | |
#              |___/                    |_|
#
# ------------------------------------------------------------------------------
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# ------------------------------------------------------------------------------
#
# -------------------------------- return --------------------------------
# 1. the string of filename with extension
def file(path):
    delimiter = findDelimiter(path)
    return path.split(delimiter)[len(path.split(delimiter)) - 1]


# -------------------------------- return --------------------------------
# 1. normally the string of the folder
# 2. if the path end with delimter return True
# 3. elif don't end with delimiter but as no extension return False
def folder(path):
    delimiter = findDelimiter(path)
    if path[len(path) - 1] == delimiter:
        return True
    elif extension(path) is False:
        return False
    else:
        folderArray = path.split(delimiter)[: len(path.split(delimiter)) - 1]
        folderString = ""
        for i in range(len(folderArray)):
            folderString += folderArray[i] + delimiter
        return folderString


# -------------------------------- return --------------------------------
# 1. normally the string of the extension
# 2. if has no extension return False
def extension(path):
    vFile = file(path)
    if len(vFile.split(".")) == 1:
        return False
    else:
        return vFile.split(".")[len(vFile.split(".")) - 1]


# -------------------------------- return --------------------------------
# 1. the filename with no extension
def filename(path):
    vFile = file(path)
    filenameArray = vFile.split(".")[: len(vFile.split(".")) - 1]
    filenameString = ""
    for i in range(len(filenameArray)):
        if i == len(filenameArray) - 1:
            filenameString += filenameArray[i]
        else:
            filenameString += filenameArray[i] + "."
    return filenameString


# find the delimiter of a given path
# -------------------------------- return --------------------------------
# 1. normally the delimiter
# 2. if delimiter not found return false
def findDelimiter(path):
    delimiter = ""
    allDelimiter = ["/", "\\"]
    for element in allDelimiter:
        if path.find(element) != -1:
            delimiter = element
    if delimiter == "":
        return False
    else:
        return delimiter
def asExtension(path):
    if extension(path) != False:
        return True
    else:
        return False