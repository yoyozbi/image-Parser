import glob
import myPythonModules.pathProperty as pathProperty

# find a given string in a given array
# -------------------------------- return --------------------------------
# 1.True, if the string is on a case of the array
# 2.False, if the string is not on a case of the array
def findStringArray(array, string):
    r = False
    for case in array:
        if case == string:
            print(case)
            print(string)
            r = True
    return r


def alreadyExist(path):
    folder = pathProperty.folder(path)
    filename = pathProperty.filename(path)
    exist = False
    if folder or folder is False:
        filesOfFolder = glob.glob(folder)
        for element in filesOfFolder:
            if element == filename:
                exist = True
        return exist
    else:
        return 1
