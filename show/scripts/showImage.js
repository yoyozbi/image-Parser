const {remote, ipcRenderer} = require("electron");
files = remote.getGlobal('sharedObj').files;
const fs = require('fs');
const path = require('path');
const runPython = require("./scripts/functions/runPython.js")
const pathToFile = remote.getGlobal('settings').pathToConvertImage;//variable set in parameters
remote.getGlobal('info').currentPage = window.location.href; //so the preload know where were at

//verify that all image exist
for (var i = 0; i < files.length; i++) 
{
  if(files[i].exists){
    alert(files[i] + ' does not exist')
  }
}


var filesToConvert = []; //contain a boolean if yes or not we need to convert an image
for (var i = 0; i < files.length; i++) 
{
  folder = files[i].split('\\')[files[i].split('\\').length - 1]
  if(folder.indexOf(".jpg" ||".tiff" || ".png" || "gif") == -1)
  {
    filesToConvert.push(files[i]);
  }
}
//setup and start thread to convert big image
remote.getGlobal("sharedObj").totalOfFileToConvert = filesToConvert.length;
remote.getGlobal("sharedObj").filesToConvert = filesToConvert;
ipcRenderer.send("message-from-main-worker","start");
//make  progressbar
var bar1 = new ldBar(".bottom");
bar1.set("0")
var bar2 = document.getElementsByClassName('bottom')[0].ldBar;

var test = setInterval(function(){
  var percent = (remote.getGlobal("sharedObj").numberOfFileConverted / remote.getGlobal("sharedObj").totalOfFileToConvert) * 100;
  bar1.set(percent);
},1000);
const imageList = document.getElementById('images');
var listOfPathToConvert =[]
for (var i = 0; i < files.length; i++) 
{

  var completePath = pathToFile + path.basename(files[i], path.extname(files[i])) + '-min.jpg';
  var imageSize = [];
  console.log(imageSize);
  var div = document.createElement('div');
  div.className = "col-lg-1 col-md-2 col-sm-12";
  imageList.appendChild(div);
  var link = document.createElement('a');
  link.href = "#"
  var image = document.createElement('img');
  runPython.convertImage(files[i])
  //see if the file exist to continue
  var asFinish = false;
  while(asFinish === false)
  {
    if(fs.existsSync(completePath))
    {
      asFinish = true;
    }
  }
  image.src = completePath;
  image.alt = completePath;
  image.className = 'img-fluid rounded';
  link.setAttribute("onclick","runPython.convertBigImage(\""+ i+"\");");
  link.appendChild(image);
  div.appendChild(link);
}
