// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const path = require('path');
const fs = require('fs');
const {ipcMain, dialog, remote} = require('electron');

global.sharedObj = {files : null,numberOfFileConverted: 0,totalOfFileToConvert:0, filesToConvert:[]};
global.settings = {pathToConvertImage: __dirname + '/images/' };
if(!fs.existsSync(global.settings.pathToConvertImage))
{
  fs.mkdirSync(global.settings.pathToConvertImage, { recursive: true })
}
global.info = {currentPage: "index.html"};
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let workerWindow
function createWindow (mainWindow) {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegrationInWorker: true,
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
    workerWindow = null
  })
  //create an hidden window for the worker
  
  workerWindow = new BrowserWindow({
    show: false,
    webPreferences:{nodeIntegration:true}
  });
  workerWindow.loadFile("worker.html")
}
function sendWindowMessage(targetWindow, message, payload) {
  if(typeof targetWindow === 'undefined') {
    console.log('Target window does not exist');
    return;
  }  targetWindow.webContents.send(message, payload);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
//open folder
ipcMain.on('open-file-dialog', (event) => {
  dialog.showOpenDialog(mainWindow,{
    properties: ['openFile','multiSelections'],
    filters: [{name:'Images', extensions:['jpg','png','gif','nef','dng','crw2']}]
  }).then(result => {
      event.sender.send('selected-directory', result.filePaths);
  })
})
ipcMain.on('message-from-thread-worker', (event, arg) => {
  sendWindowMessage(mainWindow, 'message-from-thread-worker', arg);
});
ipcMain.on('message-from-main-worker', (event, arg) => {
  sendWindowMessage(workerWindow,'message-from-main-worker',arg);
});
ipcMain.on('start', (event, arg) => {
  event.reply('start-reply',arg);
});
ipcMain.on('return', (event,arg) =>{
  event.reply('return-reply',arg);
});

