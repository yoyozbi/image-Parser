import cv2
from sklearn.metrics import mean_squared_error as mse
import os
import glob
import sys


folder = "/run/media/yohan/a0ae7f20-08ff-4e10-8b05-ae42b2e77798/photo/1Aout2019/"

os.chdir(folder)
pattern = ("*.jpeg", "*.jfif", "*.jpg", "*.NEF", "*.bmp", "*.gif", "*.svg",
"*.arw", "*.dng", "*.kdc", "*.mrw", "*.nef", "*.nrw")
fileName = []
Categories = []

mseValue = 100
for extension in pattern:
    fileName.extend(glob.glob(extension))


def findStringInArray(string, array):
    r = -1
    i = 0
    if len(array) > 0:
        while r == -1 or i < len(array)-1:
            if array[i].find(string) != -1:
                r = i
            i += 1
    print(r)
    return r


def loadImage(file1):
    image = cv2.imread(file1)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return image


def ParseImage(index, image1, image2):
    if mse(image1, image2) < mseValue:
        findImage1 = findStringInArray(fileName[index], Categories)
        if findImage1 == -1:
            Categories.append("{} - {}".format(fileName[index], fileName[index+1]))
        else:
            Categories[findImage1] += "- {}".format(fileName[index+1])
    else:
        Categories.append("{} - {}".format(fileName[index], fileName[index+1]))


def loop():
    for i in range(0, len(fileName)-2):
        image1 = loadImage(fileName[i])
        image2 = loadImage(fileName[i+1])
        ParseImage(i, image1, image2)


if(len(fileName) != 0):
    loop()
    print(Categories)
